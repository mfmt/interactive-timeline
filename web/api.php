<?php

// Accept either $_GET['id'] (normal usage) or _SERVER['argv'][1] when debugging.

require 'vendor/autoload.php';
use Michelf\Markdown;

$timeline = new Timeline();
$timeline->run();

class Timeline {

  private $id;
  private $content = [];
  private $wildcard = FALSE;
  public $debug = FALSE;
  private $db = NULL;

  public function __construct() {
    $this->initDb();
    $this->setId();
  }

  public function initDb() {
    $dbPath = 'timeline.db';
    $init = file_exists($dbPath) ? FALSE: TRUE;
    $this->db = new PDO("sqlite:{$dbPath}");
    if ($init) {
      $sql = "CREATE TABLE timeline (
        id integer UNSIGNED NOT NULL,
        date text NOT NULL CHECK (date != ''),
        title text NOT NULL CHECK (title != ''),
        img text NOT NULL,
        alt text NOT NULL,
        content text NOT NULL
      )";
      $this->db->query($sql);
    }
  }

  public function run() {
    $this->debug("Starting Run function.");
    if (!$this->id) {
      $this->id = $this->generateRandomId();
    }
    $this->processEdits();
    $this->setContentFromId();
    $this->printContent();
  }

  private function processEdits() {
    $type = $_SERVER['REQUEST_METHOD'] ?? NULL;
    if ($type == 'DELETE') {
      // We expect both a rowid and an id in the URL.
      $rowid = intval($_GET['rowid'] ?? 0);
      $id = $_GET['id'] ?? NULL;

      // We need both to delete. Otherwise someone could guess $rowids
      // and delete everything.
      if (!$rowid || !$id) {
        return;
      }

      $sql = "DELETE FROM timeline WHERE id = :id AND rowid = :rowid";
      $statement = $this->db->prepare($sql);
      $params = [':id' => $id, ':rowid' => $rowid];
      $statement->execute($params);
      $this->debug($sql);
      $this->debug(print_r($params, TRUE));
    }
    elseif ($type == 'POST') {
      $fields = [ "id", "date", "title", "img", "alt", "content" ];
      $values = [];
      $sets = [];
      foreach ($fields as $field) {
        $values[] = ":{$field}";
        $sets[] = "$field = :{$field}";
      }
      $rowid = intval($_POST['rowid'] ?? 0);
      if ($rowid) {
        $sql = "UPDATE timeline SET ";
        $sql .= implode(",", $sets);
        $sql .= " WHERE rowid = :rowid";
      }
      else {
        $sql = "INSERT INTO timeline " .
        '(' . implode(',', $fields) . ') ' .
        'VALUES(' . implode(',', $values) . ') ';
      }
      try {
        $statement = $this->db->prepare($sql);
        if ($statement === FALSE) {
          $this->debug("Nopers: $sql");
        }
        $binds = [];
        foreach ($fields as $field) {
          $binds[":{$field}"] = strip_tags($_POST[$field] ?? '');
        }
        if ($rowid) {
          $binds[":rowid"] = $rowid;
        }
        $statement->execute($binds);
      }
      catch (Exception $e) {
        $this->debug("execute failed");
        $this->debug($e->getMessage());
      }
      $this->debug($sql);
      $this->debug(print_r($binds, TRUE));
    }
  }

  private function debug($msg) {
    if ($this->debug) {
      $msg = "{$msg}\n";
      if (php_sapi_name() === "cli") {
        echo $msg;
      }
      file_put_contents('log.txt', $msg, FILE_APPEND);
    }
  }

  private function setId() {
    if (php_sapi_name() === "cli") {
        $this->debug = TRUE;
        $this->id = $_SERVER['argv'][1] ?? NULL;
        $this->debug("id is {$this->id}");
      }
      else {
        // Might come via GET or POST.
        $this->id = $_REQUEST['id'] ?? NULL;
      }
      $this->parseId();
  }

  private function parseId() {
    // The id might have a trailing * to indicate wildcard.
    $id = $this->id ?? '';
    if (preg_match('/^([a-zA-Z0-9]+)?([*])$/', $id, $matches)) {
      $this->debug(print_r($matches, TRUE));
      $this->wildcard = $matches[2] ?? FALSE;
      $this->id = $matches[1];
    }
  }


  // Thanks https://www.w3docs.com/snippets/php/how-to-generate-a-random-string-with-php.html
  private function generateRandomId() {
    $n = 10;
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }
    return $randomString;
  }

  private function printContent() {
    if (!$this->debug) {
      header("HTTP/1.1 200");
    }
    echo json_encode($this->content);
    if ($this->debug) {
      echo "\n";
    }
  }

  private function setContentFromId() {
    $this->content = [
      'id' => strip_tags($this->id),
      'events' => [],
    ];
    $id = $this->id;
    $op = '=';
    if ($this->wildcard) {
      $id = $this->id . '%';
      $op = 'LIKE';
    }
    $sql = "SELECT rowid, * FROM timeline WHERE id $op :id ORDER BY date";
    $this->debug($sql);
    $this->debug($id);
    $statement = $this->db->prepare($sql);
    $binds = [':id' => $id];
    $statement->execute($binds);
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
      $event = [];
      foreach($row as $field => $value) {
        // htmlentities is an extra precaution - html *should* be stripped before
        // insertion.
        $event[$field] = htmlentities($value);
        if ($field == 'content') {
          // markdown is allowed in content, so we create a marked up version to
          // display while keeping the original in the "content" field that is used
          // when editing.
          $event["{$field}_display"] = Markdown::defaultTransform($value);
        }
      }
      $this->content['events'][] = $event;
    }
  }
}

?>
