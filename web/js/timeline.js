(function (document) {
  console.log("Initializing timline");

  // Check for an 'id' query parameter in the URL.
  var id = new URLSearchParams(window.location.search).get("id");
  var formModal = document.getElementById('add-edit-modal');

  function loadData() {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    var query = '';
    if (id !== null) {
      query = "?id=" + id;
    }
    xobj.open('GET', 'api.php' + query, true);
    xobj.onreadystatechange = function () {
      if (xobj.readyState == 4 && xobj.status == "200") {
        // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
        displayData(xobj.responseText);
      }
    };
    xobj.send();
  }
  function deleteData() {
    var xobj = new XMLHttpRequest();
    var encodedId = encodeURIComponent(id);
    var encodedRowid = encodeURIComponent(document.getElementById("rowid").value);
    var url = 'api.php?id=' + encodeURIComponent(id) + '&rowid=' + encodedRowid;
    xobj.open('DELETE', url, true);
    xobj.onreadystatechange = function() {
      if(xobj.readyState == 4 && xobj.status == 200) {
        formModal.classList.remove('visible');
        displayData(xobj.responseText); 
      }
    }
    xobj.send();
  }

  function submitData() {
    // Thanks https://stackoverflow.com/questions/9713058/send-post-data-using-xmlhttprequest
    var xobj = new XMLHttpRequest();
    var url = 'api.php';
    var params = getFormData();
    xobj.open('POST', url, true);
    xobj.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    xobj.onreadystatechange = function() {
      if(xobj.readyState == 4 && xobj.status == 200) {
        formModal.classList.remove('visible');
        displayData(xobj.responseText); 
      }
    }
    xobj.send(params);
  }

  function getFormData() {
    encodedData = [];
    data = {}
    data["title"] = document.getElementById("title").value;
    data["date"] = document.getElementById("date-year").value + '-' +
      document.getElementById("date-month").value + '-' +
      document.getElementById("date-day").value;
    data["img"] = document.getElementById("img").value;
    data["alt"] = document.getElementById("alt").value;
    data["content"] = document.getElementById("content").value;
    data["rowid"] = document.getElementById("rowid").value;
    data["id"] = id;
    for (name in data) {
      encodedData.push(encodeURIComponent(name)+'='+encodeURIComponent(data[name]));
    }
    return encodedData.join('&');
  }

  function setFormData(data) {
    var dateParts = datePartsForDate(data['date']);
    document.getElementById("title").value = data["title"] || '';
    document.getElementById("date-year").value = dateParts["year"] || '';
    document.getElementById("date-month").value = dateParts["month"] || '';
    document.getElementById("date-day").value = dateParts["day"] || '';
    document.getElementById("img").value = data["img"] || '';
    document.getElementById("alt").value = data["alt"] || '';
    // Note: set to original content, not content_display when editing.
    document.getElementById("content").value = data["content"] || '';
    document.getElementById("rowid").value = data["rowid"] || '';
  }

  // Parse a date into it's year, month and date parts.
  function datePartsForDate(date) {
    ret = {};
    if (!date) {
      return ret;
    }
    parts = date.split('-');
    ret['year'] = parts[0];
    ret['month'] = parts[1];
    ret['day'] = parts[2];
    return ret;
  }
  function setupSubmitForm() {
    // Modal - pop up form for adding/editing events.
    var openModal = document.getElementById('open-modal');
    // Close modal button
    var closeModal = document.getElementsByClassName('close-modal')[0];
    // Open modal event listener
    openModal.addEventListener('click', function(){
      setFormData({});
      document.getElementById('event-form-delete').style.display = 'none';
      formModal.classList.toggle('visible');
    });
    // Close modal event listener
    closeModal.addEventListener('click', function(){
      formModal.classList.remove('visible');
    });
    var eventForm = document.getElementById('add-edit-event');
    eventForm.addEventListener('submit', function(e) {
      e.preventDefault();
      e.stopPropagation();
      submitData();
    });
    var eventFormDelete = document.getElementById('event-form-delete');
    eventFormDelete.addEventListener('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
      if (confirm("Are you sure?")) {
        deleteData();
      }
    });
  }

  function displayData(rawData) {
    data = JSON.parse(rawData);

    var main = document.querySelector("#timeline-main");
    main.innerHTML = "";   

    // If no id is defined via the URL, take the id from the backend and ensure
    // it gets pushed to the URL.
    if (!id) {
      id = data["id"];
      const url = new URL(window.location);
      url.searchParams.set('id', id);
      window.history.pushState({}, '', url);
    }

    // If no events have been added, add one with insructions.
    if (data.events.length == 0) {
      initialEvent = {
        "title": "Welcome!",
        "date": "2023-01-15",
        "content_display": 'Welcome to the timeline tool. You can add as many events as you want by clicking the "Add an Event" button at the top of the page. You can also share the link to this page (<span class="share-link">' + new URL(window.location) + '</span>) to collaborate on the same timeline with your friends.'
      };
      data.events.push(initialEvent);
    }

    var row = document.createElement("div");
    row.setAttribute("class", "timeline-row");;
    var count = 0;
    var horizontalLine, verticleLine, cornerTop, cornerBottom;

    // We build out rows of three events each. At the end of each row we have
    // to add some lines and corners.
    function finishRow() {
      horizontalLine = document.createElement("div");
      horizontalLine.setAttribute("class", "horizontal-line");
      verticalLine = document.createElement("div");
      verticalLine.setAttribute("class", "verticle-line");
      cornerTop = document.createElement("div");
      cornerTop.setAttribute("class", "corner top");
      cornerBottom = document.createElement("div");
      cornerBottom.setAttribute("class", "corner bottom");

      row.appendChild(horizontalLine);
      row.appendChild(verticalLine);
      row.appendChild(cornerTop);
      row.appendChild(cornerBottom);
    }
    
    // Iterate over each event, appending it to our HTML.
    data.events.forEach(function(eventObj) {
      if (count == 2) {
        // Special case when starting - since we begin in the middle of the page,
        // the first row only has two items - but we want to treat it as if it
        // has three items so we properly generate a new row.
        count = 3;
      }

      if (count != 0 && count % 3 === 0 ) {
        // Wrap up this row and begin a new one.
        finishRow();
        main.appendChild(row);

        // Initialize the next row.
        row = document.createElement("div");
        row.setAttribute("class", "timeline-row");
      }

      var paragraph = document.createElement("p");
      paragraph.innerHTML = eventObj.content_display;

      var contentText = document.createElement("div");
      contentText.setAttribute("class", "timeline-content-txt")
      contentText.appendChild(paragraph);

      var image = null;
      if (eventObj.img) {
        var image = document.createElement("img");
        image.setAttribute("src", eventObj.img);
        if (eventObj.alt) {
          image.setAttribute("alt", eventObj.alt);
        }
      }

      var content = document.createElement("div");
      content.setAttribute("class", "timeline-content");
      if (image) {
        content.appendChild(image);
      }
      content.appendChild(contentText);

      var title = document.createElement("h6");
      title.innerHTML = eventObj.title;

      var date = document.createElement("span");
      date.setAttribute("class", "timeline-date")
      date.innerHTML = eventObj.date;

      rowid = eventObj.rowid || null;
      var edit = null;
      if (rowid) {
        // There is no rowid for the intro event.
        edit = document.createElement("button");
        edit.setAttribute("class", "timeline-edit");
        edit.innerHTML = "edit";
        edit.addEventListener('click', function() {
          setFormData(eventObj);
          document.getElementById('event-form-delete').style.display = 'inline';
          formModal.classList.toggle('visible');
        });
      }

      var meta = document.createElement("div");
      meta.setAttribute("class", "timeline-meta");
      meta.appendChild(date);
      if (edit) {
        meta.appendChild(edit);
      }

      var boxWrap = document.createElement("div");
      boxWrap.setAttribute("class", "timeline-box-wrap");
      boxWrap.appendChild(title);
      boxWrap.appendChild(meta);
      boxWrap.appendChild(content);

      var box = document.createElement("div");
      var boxClass = "timeline-box logo-timeline-arrow";
      if (image) {
        boxClass += " timeline-content-img";
      }
      box.setAttribute("class", boxClass)
      box.appendChild(boxWrap);

      row.appendChild(box);
      count++;
    });

    // We are done. Now finish the last row, which has a special continue
    // button.
    var endTitle = document.createElement('h4')
    endTitle.innerHTML = 'Done';
    var end = document.createElement('div');
    end.setAttribute('class', 'start-point end-point');
    end.appendChild(endTitle);
    finishRow();
    row.appendChild(end);
    main.appendChild(row);
  }
  window.addEventListener("load", function() {
    setupSubmitForm();
    loadData(); 
  });
})(document);
